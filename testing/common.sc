using import testing

using import ..init

test
    (fixed u32 12) == (fixed u32 12)

let num1 num2 num3 =
    (fixed u8 4) 5.0
    fixedof u8 4 5.0
    5 as (fixed u8 4)

test (num1 == num2)
test (num2 == num3)

let one two =
    fixedof i16 4 1
    fixedof i16 4 2

test
    one + two == (fixedof i16 4 3)

test
    one - two == (fixedof i16 4 -1)

test
    two - one < two + one
    
let f = 
    fixed i32 8

test
    (f 1) + (f 2) * (f 3) == (f 7)

let result =
    (f 1) - (f 2) / (f 3) # about 1/3; 0.3...

test
    and
        result > (f 0.33)
        result < (f 0.34)

test
    (f 2) / (f 0.5) == (f 4)

test
    (f 2) // (f 0.5) == 4

test
    (f 0.5) / (f 2) == (f 0.25)

test
    (f 0.5) // (f 2) == 0

test
    (/ (f 4)) == (f 0.25)

test
    (/ (f 0.125)) == (f 8)

inline test-fixed (a b)
    test
        a as f == (f b)

test-fixed (fixedof u8 4 0.25) 0.25

test-fixed (fixedof u8 8 0.25) 0.25

test-fixed (fixedof i32 4 0.25) 0.25

test-fixed (fixedof i32 8 0.25) 0.25

test-fixed (fixedof i32 16 0.25) 0.25

test
    (fixedof i32 8 3.5) % (fixedof i32 8 1) == (fixedof i32 8 0.5)

test
    1 as (fixed i32 8) == (fixedof i32 8 1)

test
    ('abs (fixedof i32 8 -1)) == (fixedof i32 8 1)

