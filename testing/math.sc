using import ..init
using import testing

inline test-similar (a b delta msg)
    print a b
    test ((a - b) < delta) msg

inline tests (F i accuracy)
    print "next:" i
    test-similar
        sin i
        'sin (F i)
        accuracy
        "sin"
    test-similar
        cos i
        'cos (F i)
        accuracy
        "cos"
    test-similar
        sinh i
        'sinh (F i)
        accuracy
        "sinh"
    test-similar
        cosh i
        'cosh (F i)
        2.0 * accuracy
        "cosh"
    print;

inline test-type (F accuracy)
    print "type" F
    print;
    do
        tests F 0.0 accuracy
        tests F 1.0 accuracy
        tests F 2.0 accuracy
        tests F pi accuracy
        tests F -pi accuracy
    print
        'sqrt (F 4.0)
        'sqrt (F 2.0)
        'sqrt (F 1.0)
        'sqrt (F 0.5)
        'sqrt (F 0.25)
    print;

test-type
    fixed i32 16
    0.1

