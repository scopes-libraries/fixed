using import utils.functional

using import testing

using import ..init

fn spirv ()
    let f =
        fixed i32 12
    let vec1 =
        vectorof f 1 2 3.5

    let vec2 =
        vectorof f 3 2 1.25

    let vec-sum =
        vectorof f 4 4 4.75

    let vec-diff =
        vectorof f -2 0 2.25

    vec1 + vec2
    vec1 - vec2
    vec1 * vec2
    vec1 / vec2
    vec1 % vec2
    vec1 < vec2
    vec1 <= vec2
    vec1 > vec2
    vec1 >= vec2
    ;

compile-spirv 'fragment
    static-typify spirv

