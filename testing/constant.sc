using import ..init
using import testing

let one two =
    fixedof i32 8 1
    fixedof i32 8 2

inline test-constant? (expr message)
    test
        constant? expr
        message

test-constant? one
    "value not constant"

test-constant?
    one < two
    "comparison not constant"

test-constant?
    one + two
    "sum not constant"

test-constant?
    one - two
    "difference not constant"

test-constant?
    - one
    "negative not constant"

test-constant?
    one * two
    "product not constant"

test-constant?
    one / two
    "division not constant"

test-constant?
    one // two
    "integer division not constant"

test-constant?
    / one
    "reciprocal not constant"

test-constant?
    1 as (fixed i32 8)
    "cast not constant"


