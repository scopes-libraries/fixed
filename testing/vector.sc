using import utils.functional

using import testing

using import ..init

let f =
    fixed i32 12

let vec1 =
    vectorof f 1 2 3.5

let vec2 =
    vectorof f 3 2 1.25

let vec-sum =
    vectorof f 4 4 4.75

let vec-diff =
    vectorof f -2 0 2.25

inline test (cond)
    test cond

va-map (compose test all?)
    vec1 + vec2 == vec-sum
    vec1 - vec2 == vec-diff
    vec1 * vec2 == (vectorof f 3 4 4.375)
    vec1 / vec2 == (vectorof f (1 / 3) 1 (3.5 / 1.25))
    #vec1 // vec2 == (vectorof f (1 // 3) 1 (3.5 // 1.25))
    vec1 % vec2 == (vectorof f 1 0 1)
    vec1 < vec2 == (vectorof bool true false false)
    vec1 <= vec2 == (vectorof bool true true false)
    vec1 > vec2 == (vectorof bool false false true)
    vec1 >= vec2 == (vectorof bool false true true)

