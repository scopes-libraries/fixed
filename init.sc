using import spicetools

using import utils.functional
using import utils
using import utils.math
using import utils.transcendental

typedef fixed < immutable

fn make-fixed-type (base bits)
    let bitcount =
        'bitcount base

    @@ spice-quote
    typedef
        [(.. (tostring base) "." (tostring bits))]
        \ < fixed : base
        let
            Base = base
            Integer = [(- bitcount bits)]
            Fraction = bits

inline make-fixed-value (T value)
    let val =
        (2 ** T.Fraction) as (typeof value) * value
    bitcast (val as T.Base) T

inline promote-type (int)
    match int

    case u8 u16
    case u16 u32
    case u32 u64

    case i8 i16
    case i16 i32
    case i32 i64

    default
        error "Type not promotable"


inline fixed-mul (aT bT)
    if (aT == bT)
        let promoted =
            promote-type ('@ aT 'Base)
        spice-quote
            inline (a b)
                let ap bp =
                    promoted (storagecast a)
                    promoted (storagecast b)
                bitcast
                    ((ap * bp) >> aT.Fraction) as aT.Base
                    aT
    else `()

inline fixed-div (aT bT)
    if (aT == bT)
        let promoted =
            promote-type ('@ aT 'Base)
        spice-quote
            inline (a b)
                let ap bp =
                    promoted (storagecast a)
                    promoted (storagecast b)
                bitcast
                    ((ap << aT.Fraction) // bp) as aT.Base
                    aT
    else `()

spice fixed-type (ty args...)
    spice-match args...
    case (base as type, bits as i32)
        if (base < integer)
            make-fixed-type base bits
        else
            error "base type is required to be an integer"
    default
        error "wrong arguments"

spice fixed-value (ty args...)
    spice-match args...
    case (val : ty)
        `val
    case (val)
        if ('constant? val)
            T := ty as type
            let val =
                spice-quote
                    [(2 ** ('@ T 'Fraction) as i32)] as [('typeof val)] * val
            spice-quote
                bitcast (val as T.Base) T
        else
            `(make-fixed-value ty [val])
    case ()
        `(make-fixed-value ty 0)
    default
        error "wrong number of arguments"

run-stage;

typedef+ fixed
    inline __typecall (ty args...)
        static-if (ty == fixed)
            memocall fixed-type ty args...
        else
            fixed-value ty args...

    inline __repr (value)
        repr (value as f32)

    spice __as (vT T)
        let vT T =
            vT as type
            T as type
        let fraction =
            `(vT.Fraction as [('storageof vT)])
        if (T < integer)
            spice-quote
                inline (val)
                    (bitcast ((storagecast val) >> [fraction]) vT.Base) as T
        elseif (T < real)
            spice-quote
                inline (val)
                    ((storagecast val) as T) / ((2.0 as T) ** (vT.Fraction as T))
        elseif (T < fixed)
            if (('@ T 'Fraction) == ('@ vT 'Fraction))
                spice-quote
                    inline (val)
                        bitcast ((storagecast val) as T.Base) T
            else
                spice-quote
                    inline (val)
                        let dir =
                            T.Fraction - vT.Fraction
                        let storage =
                            if (dir < 0)
                                (storagecast val) as T.Base >> (- dir)
                            else
                                (storagecast val) as T.Base << dir
                        bitcast storage T
        else
            error "unsupported cast"

    let __rimply =
        spice-cast-macro
            fn (vT T)
                if (vT < integer or vT < real)
                    spice-quote
                        inline (val)
                            fixed-value T val
                else
                    error "unsupported cast"

    let __+ __- __neg __% __== __!= __< __<= __> __>= =
        from integer let
            \ __+ __- __neg __%
            \ __== __!= __< __<= __> __>=

    inline abs (self)
        bitcast (abs (storagecast self)) (typeof self)
    inline sqrt (self)
        let T = (typeof self)
        dump "T"
            typeof (isqrt (storagecast self))
            typeof T.Fraction
            typeof ((isqrt (storagecast self)) << T.Fraction // 2:i32)
        let result =
            bitcast ((isqrt (storagecast self)) << T.Fraction // 2) (typeof self)
        static-if (T.Fraction % 2 == 0) result
        else
            result * 2

    inline make (maker additional-accuracy)
        inline (val)
            let T = (typeof val)
            accuracy := T.Fraction // 4 + additional-accuracy
            high := T.Fraction < (sizeof T.Base) * 4
            (maker accuracy high) val

    let sin cos sinh cosh =
        make make-sin 0
        make make-cos 1
        make make-sinh 0
        make make-cosh 1

    unlet make

    inline casting (op backcast)
        fn (a b)
            let T =
                typeof a
            let ET count =
                elementof T
                countof a
            let ST =
                storageof ET
            let VST =
                vector ST count
            let result =
                op
                    bitcast a VST
                    bitcast b VST
            static-if backcast
                bitcast result T
            else result

    let
        __* = (box-pointer (spice-binary-op-macro fixed-mul))
        __/ = (box-pointer (spice-binary-op-macro fixed-div))
        __// =
            box-pointer
                simple-binary-op
                    inline (a b)
                        (storagecast a) // (storagecast b)
        __rcp =
            box-pointer
                inline (self)
                    ((typeof self) 1) / self

    let __vector+ __vector- =
        casting add true
        casting sub true

    let __vector== __vector!= =
        casting icmp== false
        casting icmp!= false

    let
        __vector* = (curry vector-map *)
        __vector/ = (curry vector-map /)
        #__vector// = (curry vector-map //)
        __vector% = (casting integer.__vector% true)
        __vector< = (casting integer.__vector< false)
        __vector<= = (casting integer.__vector<= false)
        __vector> = (casting integer.__vector> false)
        __vector>= = (casting integer.__vector>= false)

    unlet casting

inline fixedof (base bits ...)
    (fixed base bits) ...

do
    let
        fixed
        fixedof

    locals;

